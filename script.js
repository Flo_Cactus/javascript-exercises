let closureContent = document.querySelector('.closureContent');
let closureResult = document.querySelector('.closureResult');
let closureContent2 = document.querySelector('.closureContent2');
let closureResult2 = document.querySelector('.closureResult2');
let patternModule = document.querySelector('.patternModule');

// Замыкания
// Передача параметров при замыкании
// Вариант 1

function multiply(param1){
  let x = param1;
    return function(param2){return param1 * param2;}
}
let func1 = multiply(5);
let result1 = func1(6);
console.log(result1);

closureContent.innerHTML = `<span>function multiply(param1){ <br>
  let x = param1;<br>
    return function(param2){return param1 * param2;}<br>
}<br>
let func1 = multiply(5);<br>
let result1 = func1(6);<br></span>`

closureResult.innerHTML = `<h5>Результат:</h5><span>${result1}</span>`

// Вариант 2
function multiply(param3){
  let x = param3;
    return function(param4){return param3 * param4;}
}
let result = multiply(4)(5);
console.log(result);
closureContent2.innerHTML = `<span>function multiply(param3){ <br>
  let x = param3;<br>
    return function(param4){return param3 * param4;}<br>
}<br>
let result = multiply(4)(5);<br>
console.log(result);</span>`

closureResult2.innerHTML = `<h5>Результат:</h5><span>${result}</span>`;

//IIFE функции
(function (n){
  let result = 1;
  for(let i = 1; i<=n;i++)
  result *= i;
  console.log("Факториал числа "+ n +" равен " + result);
}(4));

closureResult2.insertAdjacentHTML('afterend', '<div class="IIFE"><h5>Самовызывающаяся функция</h5><br><span>(function (n){<br>let result = 1;<br>for(let i = 1; i<=n;i++) <br>result *= i;<br>console.log("Факториал числа "+ n +" равен " + result);}(4))</span></div>')

//Паттерн Модуль- пример
let calculator = (function (){
  let data = {number: 0};
    return {
      sum: function(param){
        data.number += param;
      },
      substract: function(param){
        data.number -= param;
      },
      display:function(){
        console.log("Результат: ", data.number);
      }
    }
}())
calculator.sum(10);
calculator.sum(3);
calculator.display();
calculator.substract(10);
calculator.display();

let pattern = `let calculator = (function (){ <br>
  let data = {number: 0}; <br>
    return { <br>
      sum: function(param){ <br>
        data.number += param; <br>
      }, <br>
      substract: function(param){ <br>
        data.number -= param; <br>
      }, <br>
      display:function(){ <br>
        console.log("Результат: ", data.number); <br>
      } <br>
    } <br>
}()) <br>
calculator.sum(10); <br>
calculator.sum(3); <br>
calculator.display(); <br>
calculator.substract(10); <br>
calculator.display(); <br>`

patternModule.insertAdjacentHTML('beforeend','<div class="module">'+pattern+'</div> <h5>Просмотр результата- в консоли</h5>');

//Рекурсия
//пример с нахождением факториала числа
function getFactorial(n){
  this.n = n;
  if(n === 1 || n ===0){
    return 1;
  }else{
    return n*getFactorial(n-1);
  }
}
let res = getFactorial(4);
console.log("Факториал числа "+ n +" равен " + res);
let res1 = getFactorial(1);
console.log("Факториал числа "+ n +" равен " + res1);
let res2 = getFactorial(0);
console.log("Факториал числа "+ n +" равен " + res2);

//Рассмотрим другой пример - определение чисел Фибоначчи:
function getFibonacci(n){
  if(n === 0) return 0;
  if(n === 1) return 1;
  if(n !==1 && n !==0) return getFibonacci(n-1) + getFibonacci(n-2);
}
let resFib = getFibonacci(8);//передаем порядковый номер в последоательности Фиббоначи 8й по счету = 21
console.log(resFib);
